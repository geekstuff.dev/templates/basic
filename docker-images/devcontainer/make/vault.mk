#make

VAULT_ADDR ?=
VAULT_NAMESPACE ?= default

VAULT_AUTH_METHOD ?= oidc
VAULT_AUTH_PATH ?= oidc
VAULT_AUTH_ROLE ?=

.PHONY: vault-login
vault-login: .vault-login

.PHONY: vault-login
vault-login:
	@echo "[] Login to Vault with auth method and path ${VAULT_AUTH_METHOD} / ${VAULT_AUTH_PATH}, in NS ${VAULT_NAMESPACE} and role ${VAULT_AUTH_ROLE}"
	VAULT_NAMESPACE=${VAULT_NAMESPACE} vault login -method=${VAULT_AUTH_METHOD} -path=${VAULT_AUTH_PATH} $(shell if test -n "${VAULT_AUTH_ROLE}"; then echo "role=${VAULT_AUTH_ROLE}"; fi)

.PHONY: vault-check
vault-check:
	@vault token lookup 1>/dev/null 2>/dev/null || { echo "Not logged into Vault"; false; }

.PHONY: vault-token-lookup
vault-token-lookup:
	vault token lookup

.PHONY: vault-token-revoke
vault-token-revoke:
	vault token revoke -self
