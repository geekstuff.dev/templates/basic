#!make

.PHONY: info
info:
	@echo "Basic template"
	@echo ""
	@echo "Commands to try:"
	@echo "  make template.diff"
	@echo ""
	@echo "Devcontainer features:"
	@echo "  - Use TAB to auto-complete or discover make commands"
	@echo "  - Within a devcontainer, you can use VSCode to browse other files:"
	@echo "    'code /devcontainer-lib/'"
