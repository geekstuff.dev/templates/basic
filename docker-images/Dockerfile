ARG FROM_PREFIX
ARG SOURCE_IMAGE
ARG SOURCE_TAG

##### Stage: base - serves both devcontainer and gitlab-ci stages
FROM ${FROM_PREFIX}${SOURCE_IMAGE}:${SOURCE_TAG} as base
ENV TPL_IMG="base"

# Use these args within this stage
ARG BUILD_TAG
ARG CI_PROJECT_URL
ARG SOURCE_IMAGE
ARG SOURCE_TAG
ARG PROJECT_TAG
ARG VARIANT

# Set as ENV vars
ENV BUILD_TAG=${BUILD_TAG}
ENV TPL_PROJECT_SOURCE_TAG=${SOURCE_TAG}
ENV TPL_PROJECT_SOURCE_IMAGE=${SOURCE_IMAGE}
ENV TPL_PROJECT_TAG=${PROJECT_TAG}
ENV TPL_SOURCE_URL=${CI_PROJECT_URL}/-/tree/${PROJECT_TAG}

# Install dependencies
RUN set -e && \
    if command -v apk 2>/dev/null 1>/dev/null; then \
        echo "installing APK dependencies"; \
        apk add --update --no-cache curl diffutils grep unzip zip; \
    elif command -v apt 2>/dev/null 1>/dev/null; then \
        echo "installing APT dependencies"; \
        apt-get update; \
        apt-get install -y curl unzip zip; \
        rm -rf /var/lib/apt/lists/*; \
    fi

##### Stage: devcontainer
FROM base as devcontainer
ENV TPL_IMG="devcontainer"

ARG CI_PROJECT_URL
ARG CI_REGISTRY_IMAGE
ARG SPECIFIC_IMAGE

# Copy devcontainer scripts
COPY --from=geekstuffreal/devcontainer:v0.14 /scripts/ /devcontainer/

# Setup devcontainer
RUN /devcontainer/basics.sh

# Setup tools
RUN /devcontainer/tools/direnv.sh
RUN /devcontainer/tools/docker.sh

# Setup user
RUN /devcontainer/user/starship.sh

# For linux commands, set VSCode as the default editor.
ENV EDITOR="code --wait"
ENV VISUAL="$EDITOR"

# Copy script to make devcontainer web browser calls work
COPY ./docker-images/devcontainer/xdg-open.sh /usr/local/bin/xdg-open

# Install latest Vault cli
COPY --from=hashicorp/vault /bin/vault /bin/

# Lib ENV vars
ARG TPL_LIB=/devcontainer-lib
ENV TPL_LIB=${TPL_LIB}
ENV TPL_LIB_DIRENV=${TPL_LIB}/direnv
ENV TPL_LIB_MAKE=${TPL_LIB}/make
ENV TPL_SPECIFIC_IMAGE=${SPECIFIC_IMAGE}
ENV TPL_TEMPLATES=${TPL_LIB}/template

# Copy devcontainer scripts
COPY ./docker-images/devcontainer/direnv/ ${TPL_LIB_DIRENV}/
COPY ./docker-images/devcontainer/make/ ${TPL_LIB_MAKE}/

# Template users should use
COPY ./user-template/ ${TPL_TEMPLATES}/
COPY ./scripts/template-bundler.sh /tpl-utils/template-bundler.sh
RUN TEMPLATE_VARIATION=${VARIANT} /tpl-utils/template-bundler.sh ${TPL_TEMPLATES} \
    && rm -rf /tpl-utils

# Setup ONBUILD to auto apply user gitconfig overrides if any were passed
ONBUILD ARG GIT_EMAIL
ONBUILD ARG GIT_SIGNINGKEY
ONBUILD RUN GIT_EMAIL=${GIT_EMAIL} \
    GIT_SIGNINGKEY=${GIT_SIGNINGKEY} \
        /devcontainer/basics/4-gitconfig.sh
