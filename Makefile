#!make

# NOTE: This Makefile is made with local dev usage in mind

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

CI_PROJECT_URL ?= https://gitlab.com/geekstuff.dev/templates/basic
CI_REGISTRY_IMAGE ?= tpl/basic

SOURCE_IMAGE_DEBIAN ?= debian
SOURCE_TAG_DEBIAN ?= bullseye-slim
SOURCE_IMAGE_ALPINE ?= alpine
SOURCE_TAG_ALPINE ?= 3.17

TEMPLATE_ALPINE_PKG ?= basic_alpine_${PROJECT_TAG}
TEMPLATE_DEBIAN_PKG ?= basic_debian_${PROJECT_TAG}

PROJECT_TAG ?= v0.0.0
PACKAGE_DIR ?= packages

all: info

.PHONY: info
info:
	@echo "Welcome to ${CI_PROJECT_URL}"
	@echo ""
	@echo "Useful commands:"
	@echo "  make alpine.docker.devcontainer"
	@echo "  make debian.docker.devcontainer"

# IMPORTANT: This job is used in CI on top of dev
.PHONY: .docker
.docker:
	docker build \
		-t ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${SOURCE_IMAGE}-${SOURCE_TAG} \
		-f docker-images/Dockerfile \
		--target ${SPECIFIC_IMAGE} \
		--cache-from ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${SOURCE_IMAGE}-${SOURCE_TAG} \
		--cache-from ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/main:${SOURCE_IMAGE}-${SOURCE_TAG} \
		--build-arg CI_PROJECT_URL=${CI_PROJECT_URL} \
		--build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
		--build-arg SOURCE_IMAGE=${SOURCE_IMAGE} \
		--build-arg SOURCE_TAG=${SOURCE_TAG} \
		--build-arg PROJECT_TAG=${PROJECT_TAG} \
		--build-arg FROM_PREFIX=${FROM_PREFIX} \
		--build-arg SPECIFIC_IMAGE=${SPECIFIC_IMAGE} \
		--build-arg VARIANT=${VARIANT} \
		.
	@docker images ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${SOURCE_IMAGE}-${SOURCE_TAG}

.PHONY: .docker.devcontainer
.docker.devcontainer:
	@SPECIFIC_IMAGE=devcontainer $(MAKE) .docker
	docker run --rm ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/devcontainer:${SOURCE_IMAGE}-${SOURCE_TAG} \
		cat /devcontainer-lib/template/.devcontainer/Dockerfile

.PHONY: alpine.docker.devcontainer
alpine.docker.devcontainer:
	@SOURCE_IMAGE=${SOURCE_IMAGE_ALPINE} \
		SOURCE_TAG=${SOURCE_TAG_ALPINE} \
		VARIANT=alpine \
		$(MAKE) .docker.devcontainer

.PHONY: debian.docker.devcontainer
debian.docker.devcontainer:
	@SOURCE_IMAGE=${SOURCE_IMAGE_DEBIAN} \
		SOURCE_TAG=${SOURCE_TAG_DEBIAN} \
		$(MAKE) .docker.devcontainer

.PHONY: .template
.template:
	@CI_PROJECT_URL=${CI_PROJECT_URL} \
	CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
	SOURCE_IMAGE=${SOURCE_IMAGE} \
	SOURCE_TAG=${SOURCE_TAG} \
	PROJECT_TAG=${PROJECT_TAG} \
	SPECIFIC_IMAGE=devcontainer \
	TEMPLATE_VARIATION=${TEMPLATE_VARIATION} \
		scripts/template-bundler.sh \
			user-template/ \
			${PACKAGE_DIR}/${OUTPUT_FILE}

.PHONY: alpine.template.apply
alpine.template.apply:
	@SOURCE_IMAGE=${SOURCE_IMAGE_ALPINE} \
		SOURCE_TAG=${SOURCE_TAG_ALPINE} \
		TEMPLATE_VARIATION=alpine \
		$(MAKE) .template

.PHONY: debian.template.apply
debian.template.apply:
	@SOURCE_IMAGE=${SOURCE_IMAGE_DEBIAN} \
		SOURCE_TAG=${SOURCE_TAG_DEBIAN} \
		TEMPLATE_VARIATION=debian \
		$(MAKE) .template

.PHONY: all.template.packages
all.template.packages: alpine.template.package debian.template.package

.PHONY: alpine.template.package
alpine.template.package:
	@mkdir -p ${PACKAGE_DIR}
	@OUTPUT_FILE=${TEMPLATE_ALPINE_PKG} \
		$(MAKE) alpine.template.apply

.PHONY: debian.template.package
debian.template.package:
	@mkdir -p ${PACKAGE_DIR}
	@OUTPUT_FILE=${TEMPLATE_DEBIAN_PKG} \
		$(MAKE) debian.template.apply

.PHONY: test
test: test.setup

.PHONY: test.setup
test.setup: test.cleanup
	@$(MAKE) alpine.docker.devcontainer debian.docker.devcontainer test.template.package.usage

.PHONY: test.template.restore
test.template.restore:
	git restore user-template/

.PHONY: test.cleanup
test.cleanup:
	@rm -rf ${PACKAGE_DIR}

.PHONY: test.template.package.usage
test.template.package.usage: test.cleanup
	@mkdir -p ${PACKAGE_DIR}/test-alpine ${PACKAGE_DIR}/test-debian
	@$(MAKE) alpine.template.package debian.template.package
	@tar -C ${PACKAGE_DIR}/test-alpine -zxf ${PACKAGE_DIR}/${TEMPLATE_ALPINE_PKG}.tar.gz 1>/dev/null
	@sh -c "cd ${PACKAGE_DIR}/test-debian && unzip ../${TEMPLATE_DEBIAN_PKG}.zip" 1>/dev/null
	@sh -c "cd ${PACKAGE_DIR}/test-alpine && git init && git add . && git status && git commit -m 'Initial commit'" 1>/dev/null
	@sh -c "cd ${PACKAGE_DIR}/test-debian && git init && git add . && git status && git commit -m 'Initial commit'" 1>/dev/null
	@echo "Tests ready at ${PACKAGE_DIR}/test-{alpine,debian}"

# For CI usage

.PHONY: .ci.docker.build
.ci.docker.build: .ci.docker.pull .docker

.PHONY: .ci.docker.pull
.ci.docker.build.pull:
	docker pull ${CI_IMAGE} 2>/dev/null || true
	docker pull ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/main:${SOURCE_IMAGE}-${SOURCE_TAG} 2>/dev/null || true

.PHONY: .ci.docker.push
.ci.docker.push:
	docker push ${CI_IMAGE} || true

.PHONY: ci.docker.alpine
ci.docker.alpine: SPECIFIC_IMAGE = devcontainer
ci.docker.alpine: CI_IMAGE = ${CI_REGISTRY_IMAGE}/${PROJECT_TAG}/${SPECIFIC_IMAGE}:${SOURCE_IMAGE_ALPINE}-${SOURCE_TAG_ALPINE}
ci.docker.alpine: SOURCE_TAG = ${SOURCE_TAG_ALPINE}
ci.docker.alpine: SOURCE_IMAGE = ${SOURCE_IMAGE_ALPINE}
ci.docker.alpine: .ci.docker.build .ci.docker.push
	@echo "The following docker image was built and pushed:"
	@echo "\e[44m${CI_IMAGE}\e[0m"

.PHONY: ci.template.packages
ci.template.packages:
	@mkdir -p ${PACKAGE_DIR}
	@SOURCE_TAG=${SOURCE_TAG_ALPINE} OUTPUT_FILE=${TEMPLATE_ALPINE_PKG} $(MAKE) alpine.template.apply
	@SOURCE_TAG=${SOURCE_TAG_DEBIAN} OUTPUT_FILE=${TEMPLATE_DEBIAN_PKG} $(MAKE) debian.template.apply
