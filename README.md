# Basic devcontainer template

Basic project template with batteries included.

## How to use

- Open the latest [gitlab release](https://gitlab.com/geekstuff.dev/templates/basic/-/releases)
- Follow the instructions
- Open folder with VSCode
    - Install the recommended [remote container extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
    - A `Reopen in container` popup should appear
    - Otherwise F1, search and run `Reopen in Container`

## TODO

- See what to do for gitlab-ci portion
  - maybe offer some variants
  - maybe auto-devops can help in generic project too
