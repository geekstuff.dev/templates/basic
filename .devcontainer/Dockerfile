# NOTE: The devcontainer image this project produces, is also exactly
#       what this project own devcontainer needs.
#       Ideally we should find a way to combine both.

FROM alpine:3.17.0

# Specify through the git ignored .devcontainer/.env file
ARG GIT_EMAIL
ARG GIT_SIGNINGKEY

# basic necessities
RUN apk add --update --no-cache curl diffutils grep unzip zip

# Copy devcontainer scripts
COPY --from=geekstuffreal/devcontainer:v0.14 /scripts/ /devcontainer/

# Setup devcontainer
RUN GIT_EMAIL=${GIT_EMAIL} \
    GIT_SIGNINGKEY=${GIT_SIGNINGKEY} \
        /devcontainer/basics.sh

# Setup tools
RUN /devcontainer/tools/direnv.sh
RUN /devcontainer/tools/docker.sh

# Setup user
RUN /devcontainer/user/starship.sh

# For linux commands, set VSCode as the default editor.
ENV EDITOR="code --wait"
ENV VISUAL="$EDITOR"

# Help browser launches within devcontainer
COPY ./xdg-open.sh /usr/local/bin/xdg-open

# Switch to dev user
USER dev
WORKDIR /home/dev
